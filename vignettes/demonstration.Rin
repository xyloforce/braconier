#' ---
#' title: "Package introduction"
#' author: s. barreto
#' date: "2021-03-23"
#' output:
#'   distill::distill_article:
#'     code_folding: true
#'     toc: true
#'     theme: theme.css
#' vignette: >
#'   %\VignetteIndexEntry{demonstration}
#'   %\VignetteEngine{knitr::rmarkdown}
#'   %\VignetteEncoding{UTF-8}
#'
#' ---
#'

#- r knitr_init, echo=FALSE, cache=FALSE -----------------------------
knitr::opts_chunk$set(
  tidy=FALSE,
  cache=FALSE,
  fig.width=7,
  fig.height=5,
  dev="svg")

library(ggplot2)
hrbrthemes::update_geom_font_defaults(hrbrthemes::font_ps_light)
theme_set(hrbrthemes::theme_ipsum_ps() +
            theme(
              panel.background = element_rect(fill = gray(0.95), color = gray(0.7)),
              plot.background = element_rect(fill = gray(0.975), color = gray(1))))

#' # Introduction
#'
#' The braconieR package is intended as a pipeline for annotating
#' Bracoviral regions in wasp genomes. This vignette demonstrates how it
#' should be used. For this we will use the genome of *Cotesia
#' congregata*, and well described and assembled wasp genome as a query,
#' to try to locate clusters of proviral coding sequences in the genome
#' of *Microplitis demolitor*, another well assembled wasp genome. The
#' two species are reported to have diverged 43 Mya.
#'
#' The principle is the following: amino-acid sequences for the genes
#' of the first proviral segment of *Cc* are blasted (tblastn) against
#' the two targets (positive and distant control being Cc and Md
#' respectively), using the
#' [biocblast](https://gitlab.com/bacterial-gbgc/biocblast) interface
#' to NCBI's blast CLI. The
#' [blastClust](https://gitlab.com/hrz-project/blastclust) package is
#' then used to cluster blast hits in regions. We will also see how
#' this approach competes with another clustering method (using
#' [fcScan](https://bioconductor.org/packages/release/bioc/html/fcScan.html)).
#'
#' Previous work have already delimited the segments of Md and Cc, so we
#' can use it to define a ground truth to compare to.
#'

#- library, cache=FALSE ----------------------------------------------

library(biocblast)
library(blastClust)
library(braconieR)
library(magrittr)
library(data.table)
library(GenomicRanges)
library(Biostrings)
library(genbankr)
library(plyranges)

library(ggplot2)
library(hrbrthemes)
hrbrthemes::update_geom_font_defaults(font_ps_light)
theme_set(hrbrthemes::theme_ipsum_ps() +
            theme(
              panel.background = element_rect(fill = gray(0.95), color = gray(0.7)),
              plot.background = element_rect(fill = gray(0.975), color = gray(1))))

#- databases ---------------------------------------------------------
tmp <- tempdir()
dir.create(tmp, FALSE)
fp <- function(x) file.path(tmp, x)

# cotesia congregata genome
cc_gnm <- fp("cc.fasta")
# microplitis demolitor genome
md_gnm <- fp("md.fasta")

#--- cc_gnm_fetch, eval=FALSE
## this will download the genome (around 170 MB; might take long.)
fetch_gnm(cc_gnm, "https://bipaa.genouest.org/sp/cotesia/download/genome/cotesia_congregata/v2.0/chromosome_scale_assembly_scaffolds.fasta")

#--- cc_gnm, include=FALSE
file.copy("/Users/samuelbarreto/hrz/p/bu_fabien-bv-clustering_2021-03-23/data/cc.fasta", cc_gnm)

#--- md_gnm_fetch, eval=FALSE
efetch(md_gnm, "GCF_000572035.2")

#--- md_gnm, include=FALSE
file.copy("/Users/samuelbarreto/hrz/p/bu_fabien-bv-clustering_2021-03-23/data/md.fasta", md_gnm)

#'

# 3. fetch cotesia circles
## circle
efetch(fp("cc_bv.1.fna"), "AJ632304.1", "fasta")
## annotation in genbank format
efetch(fp("cc_bv.1.gbk"), "AJ632304.1", "gb")


#--- blastdb
cc_db <- blastDb(DNAStringFile(fp("cc.fasta")), blastDbParam("nucl"))
md_db <- blastDb(DNAStringFile(fp("md.fasta")), blastDbParam("nucl"))

## blast circle against its own genome
circle1_hits <-
  biocblast::blast(DNAStringFile(fp("cc_bv.1.fna")), cc_db)[[1]]
circle1_hits <- circle1_hits[order(circle1_hits$length, decreasing=TRUE)]

#' The width of circle 1 from start to end is `r width(readDNAStringSet(fp("cc_bv.1.fna")))` bp.

#--- blast_drj

# Cotesia congregata segment DRJs are embedded in the package
drj_fna <- system.file("extdata", "cc_bv.drj.fna", package = "braconieR")
circle1_drj <- read_dna(drj_fna, "Seg1-")
circle1_drj_hits <- blast(circle1_drj, cc_db)

## truth coordinate of Cc circle 1
cc_bv_1 <- range(unlist(circle1_drj_hits))

#' I defined the “true” coordinates of circle 1 as the region from DRJ
#' 5' to DRJ 3' as given by their non-ambiguous blast hits. Next I
#' blasted the amino-acid sequence of all CDS of circle 1 (as stored
#' in their genbank file) against the genome using tblastn (query =
#' AA, subject = DNA).
#'

## blast cds against cc
cc_bv_1.cds <- cds(readGenBank(fp("cc_bv.1.gbk")))
cc_bv_1.aa <- read_aa(fp("cc_bv.1.gbk"))
cc_bv_1.aa_hits <- blast(cc_bv_1.aa, cc_db)

##' # Clustering blast hits using fcScan

## reformat hits for fcScan::getCluster to work.
all_hits <- unlist(cc_bv_1.aa_hits)
all_hits$site <- names(all_hits)

all_hits_qual_flt <- all_hits %>%
  ## split by CDS
  split(all_hits$site) %>%
  ## endoapply is endomorphic lapply (return object of same class)
  endoapply(function(x) {
    ##                  keep either all hits or only 10 if there are
    ##                  more than 10 hits.
    x[order(x$e_value)][seq(1, min(10, length(x)))]
  }) %>%
  unlist()

## cluster along the genome using all unfiltered hits
clst <- fcScan::getCluster(
  all_hits,
  w = width(cc_bv_1),
  c = setNames(rep(1, length(cc_bv_1.aa)), names(cc_bv_1.aa)),
  greedy = TRUE)

## do the same using filtered hits.
clst_qual_flt <- fcScan::getCluster(
  all_hits_qual_flt,
  w = 1e5,
  c = setNames(rep(1, length(cc_bv_1.aa)), names(cc_bv_1.aa)),
  greedy = TRUE)


#--- fig001, fig.cap="Unfiltered blast hits", fig.height=3

c(clst, cc_bv_1) %>%
  mutate(description = c("cluster", "cluster", "circle")) %>%
  plot()

#--- fig002, fig.cap="Filtered blast hits", fig.height=3

c(clst_qual_flt, cc_bv_1) %>%
  mutate(description = c("cluster", "circle")) %>%
  plot()

#' Figures \ref{fig:fig001} and \ref{fig:fig002} show that filtering
#' blast hits based on their quality (e-value ordering) is enough to
#' remove spurious matches on the positive control (Cc). Now for the
#' *Microplitis demolitor* genome.
#'
#' Next I control against Md genome.

# tblastn against Md genome
cc_bv_1.aa_hits_md <- blast(cc_bv_1.aa, md_db)

cluster_hits_fc(fp("cc_bv.1.gbk"), md_db)

#' No cluster were found for this circle. I thus tried with another
#' circle (circle 19).

#--- fetch19, include=FALSE
efetch(fp("cc_bv.19.gbk"), "AJ632321.1", "gb")
efetch(fp("cc_bv.19.fna"), "AJ632321.1", "fasta")

##- tab001, results="asis"
## against cc
gkable(cluster_hits_fc(fp("cc_bv.19.gbk"), cc_db) %>%
         range(),
       caption = "Cluster found for circle 19 against Cc",
       label = "001")

#' Table \ref{tab:001} shows hits against Cc genome.

## /* WIP

## ## against C. typhae
## ct_gnm <-
## ct_db <- blastDb(DNAStringFile(fp("ct.fasta")), blastDbParam("nucl"))
## cluster_hits_fc(fp("cc_bv.19.gbk"), ct_db)
#' No hits were found on Cotesia typhae.

##  */

## against md
cluster_hits_fc(fp("cc_bv.19.gbk"), md_db)

#' No hits were found on Md.
#'
#' # Distance-based clustering
#'
#' As no satisfying clustering method could be found to be working on
#' Md, I coded up another clustering method.

## quality filtered blast hits against Cc
d <- all_hits_qual_flt

## remove empty slots
d <- Filter(Negate(is_empty), split(d, seqnames(d)))

## test against the ground truth
d1 <- d$contig_2034

##- fig000, fig.cap="Distribution of between-CDS distance of two circles"
hist(c(mcols(distanceToNearest(cc_bv_1.cds))$distance,
       mcols(distanceToNearest(cds(readGenBank(fp("cc_bv.19.gbk")))))$distance),
     main = "", xlab = "distance between CDS")

#--- tab002, results="asis"
d1 %>%
  cluster_hits_1() %>%
  gkable(caption = "Cluster found for circle 1",
         label = "002")

cc_bv_clst <- unlist(cluster_hits(cc_bv_1.aa_hits))
#--- tab003,results="asis"
gkable(cc_bv_clst, caption =
       "Cluster found for circle 1 against all contigs. Does contig 2030 contains a circle?")

#--- fig006, fig.cap="Cluster found for circle 1 against all contigs. Does contig 2030 contains a circle?", fig.height=3

c(cc_bv_1, cc_bv_clst) %>%
  mutate(description = c("Segment 1", "cluster", "cluster")) %>%
  plot()

## control against Md
hits_md <- cluster_hits(cc_bv_1.aa_hits_md)

#--- fig003, fig.cap="Detected clusters in Md", fig.height=3
plot(unlist(hits_md) %>% mutate(description = "detected cluster"))

#' Clusters were found in Md. Do they correspond to true circles?

truth_md <- fread(system.file("extdata", "md_bv.gff", package = "braconieR"), skip = 1)[
, .(seqnames = V1, start = V4, end = V5, id = V9)]
truth_md <- as(truth_md, "GRanges")

hits_md_r <- unlist(hits_md)

#--- fig005, fig.cap="Overlap between found clusters and true circles in Md.", fig.height=3

mergeSeqLevels(truth_md, hits_md_r, subsetByOverlaps) %>%
  c(., hits_md_r) %>%
  mutate(id = ifelse(is.na(id), "cluster", id)) %>%
  mutate(description = id) %>%
  plot()

#' # How to use the code?
#'
#' The workflow is simple:
#'
#' - blast AA sequence of CDS against a target genome.
#' - cluster hits by contigs, removing small clusters and clusters supported by too few CDS.
#' - apply this procedure to all contigs
#' - tally report and eventual quality filtering
#' - compare against ground truth
#'
#' Here is an example:

#--- codex, include=TRUE, echo=TRUE, eval=FALSE

library(biocblast)
library(genbankr)

cc_db <- blastDb(DNAStringFile(fp("cc.fasta")), blastDbParam("nucl"))
segment_1_aa <- read_aa(fp("cc_bv.1.gbk"))

## blast DRJ
circle1_drj <- read_dna(drj_fna, "Seg1-")
circle1_drj_hits <- (blast(circle1_drj, cc_db))
## truth coordinate of Cc circle 1
cc_bv_1 <- range(unlist(circle1_drj_hits))

cc_hits  <- blast(segment_1_aa, cc_db)
cc_hits_clst <- cluster_hits(cc_hits)

## is cluster on true coordinates?
unlist(cc_hits_clst) %over% cc_bv_1

## plot
c(
  cc_bv_1 %>% mutate(description = "Segment"),
  unlist(cc_hits_clst) %>% mutate(description = "Clusters")
) %>%
  plot()

## Local Variables:
## mode: ess-r
## End:
