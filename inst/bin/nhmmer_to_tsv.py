#!/usr/bin/env python3
import sys

def hmmer_to_csv(hmmer_file, tsv_result, header = False):
    import csv
    import re

    hmmerh = open(hmmer_file)
    csvh = open(tsv_result, "w")
    csv_w = csv.writer(csvh, dialect = "excel")

    line = hmmerh.readline()
    if header:
        csv_w.writerow(re.findall("# (\w+ *\w+) *(\w+) *(\w+ \w+) *(\w+) *(\w+) *(\w+ \w+) *(\w+) *(\w+ \w+) *(\w+) *(\w+ \w+) *(\w+ \w+) *(\w+) *(\w-\w+) *(\w+) *(\w+) *(\w+ \w+ \w+)", line)[0])

    for line in hmmerh.readlines():
        if not line.startswith("#"):
            regex = list(re.findall("([\w.]+) *(-) *(\w.+) *(-) *(\d+) *(\d+) *(\d+) *(\d+) *(\d+) *(\d+) *(\d+) *([+-]) *([\w.-]+) *([\d.]+) *([\d.]+) *(.*)$", line)[0])
            if(regex[11] == "-"):
                temp = regex[7]
                regex[7] = regex[6]
                regex[6] = temp
            csv_w.writerow(regex)
    csvh.close()

if len(sys.argv) == 3:
    hmmer_to_csv(sys.argv[1], sys.argv[2])
else:
    raise Exception("Incorrect number of arguments")