# prepare the package for release
PKGNAME := $(shell sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS := $(shell sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGSRC  := $(shell basename `pwd`)

all: check clean

deps:
	# tlmgr install pgf preview xcolor;\
	# Rscript -e 'if (!require("Rd2roxygen")) install.packages("Rd2roxygen", repos="http://cran.rstudio.com")'

.PHONY: site
site: public/index.html

public/index.html: vignettes/demonstration.html
	@mkdir -p public
	cp $< public/index.html

.PHONY: docs
docs:
	R -q -e 'devtools::document()'

build:
	cd ..;\
	R CMD build --no-manual $(PKGSRC)

build-cran:
	cd ..;\
	R CMD build $(PKGSRC)

install: build
	cd ..;\
	R CMD INSTALL $(PKGNAME)_$(PKGVERS).tar.gz

check: build-cran
	cd ..;\
	R CMD check $(PKGNAME)_$(PKGVERS).tar.gz --as-cran


# examples:
.PHONY: vignettes
vignettes:
	$(MAKE) -C vignettes

vignettes/demonstration.html:
	$(MAKE) -C vignettes demonstration.html

clean:
	cd ..;\
	$(RM) -r $(PKGNAME).Rcheck/
